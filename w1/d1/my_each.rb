def double_array(array)
  array.map {|num| num * 2}
end

class Array
  def my_each(&block)

    0.upto(self.length - 1) do |position|
      block.call(self[position])
    end

    self
  end
end

p double_array([1,2,3,4])

# calls my_each twice on the array, printing all the numbers twice.
return_value = [1, 2, 3].my_each do |num|
  puts num
end.my_each do |num|
  puts num
end

p return_value # => [1, 2, 3]