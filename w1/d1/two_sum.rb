class Array
  def two_sum
    pairs = []
    self.each_with_index do |first_num, first_idx|
      self.each_with_index do |second_num, second_idx|
        if first_num + second_num == 0
          pairs << [first_idx, second_idx].sort unless first_idx == second_idx
        end
      end
    end

    pairs.uniq
  end
end