class Game
  MAX_TURNS = 10

  def initialize
    @code = Code.new.code
  end

  def run
    turn = 1
    puts "Make initial guess:"
    current_guess  = parse_guess(gets.chomp)
    interpret_guess(current_guess)

    until won?(current_guess) || turn == MAX_TURNS
      puts "Make a guess:"
      current_guess = get_guess
      interpret_guess(current_guess)
      turn += 1
    end

    puts "You won!" if won?(current_guess)
    puts "Out of turns." if turn == MAX_TURNS
    p turn
    p current_guess
  end

  private

  def get_guess
    current_guess = parse_guess(gets.chomp)
  end

  def parse_guess(guess_string)
    guess_string.split("")
  end

  def right_colors(guess_array)
    guess_array.select { |color| @code.include?(color) }.count
  end

  def right_positions(guess_array)
    right_positions = []

    guess_array.each_index do |index|
      if guess_array[index] == @code[index]
        right_positions << guess_array[index]
      end
    end
    right_positions.count
  end

  def interpret_guess(guess_array)
      puts "#{right_colors(guess_array)} correct colors."
      puts "#{right_positions(guess_array)} right positions."
  end

  def won?(current_guess)
    current_guess == @code
  end

end

class Code
  attr_accessor :code

  def initialize
    @colors = ["r", "g", "b", "y", "o", "p"]
    @code = @colors.sample(4)
  end

end