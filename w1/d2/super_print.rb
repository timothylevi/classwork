def super_print(str, options = {})
  defaults = {
    :times   => 1, 
    :upcase  => false,
    :reverse => false
  }
  
  options = defaults.merge(options)
  
  str = options[:times].times {p str} if options[:times] > 1
  str = str.upcase if options[:upcase] == true
  str = str.reverse if options[:reverse] == true
  
  p str
end