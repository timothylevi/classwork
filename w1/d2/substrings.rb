def substrings(str)
  array = get_substrings(str)
  subwords(array)
end

def get_substrings(str)
  substring_arr = []
  
  letters = str.split("")
  
  (0...letters.size).each do |i|
    (i...letters.size).each do |j|
      sub = letters[i..j].join
      substring_arr.include?(sub) ? next : substring_arr << sub
    end
  end    
  
  substring_arr
end

def subwords(substring_arr)
  subwords_arr = []
  
  File.foreach("dictionary.txt") do |line|
    substring_arr.each do |substring|
      line.chomp == substring ? subwords_arr << substring : next
    end
  end
  
  subwords_arr
end

